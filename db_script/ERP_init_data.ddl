-- 選單(系統管理)
DELETE FROM t_erp_function;
ALTER TABLE t_erp_function AUTO_INCREMENT = 1;

INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('1', '系統管理', '0', NULL, '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('2', '權限管理', '1', '/permission/roleList', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('3', '帳號管理', '1', '/permission/userList', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('4', '修改密碼', '1', '/permission/updateUserPwdInit', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('5', '菜單管理', '1', '/function/init', '0', '1');
-- 選單(基本資料)
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('6', '基本資料', '0', NULL, '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('7', '公司資料', '6', '/company/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('8', '部門資料', '6', '/department/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('9', '職稱資料', '6', '/jobTitle/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('10', '員工資料', '6', '/employee/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('11', '遊戲資料', '6', '/gameData/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('12', '金流資料', '6', '/payType/init', '0', '1');
-- 選單(表單資料)
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('13', '表單資料', '0', NULL, '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('14', '測試單', '13', '/qcForm/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('15', '請款單', '13', '/paymentForm/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('16', '公司卡消費通知單', '13', '/businessCardForm/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('17', '退款申請單', '13', '/refundForm/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('18', '出差申請單', '13', '/businessTripForm/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('19', '出差報支單', '13', '/businessTripExpenseForm/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('20', '部門固定成本預算表', '13', '/budgetForm/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('21', '重大事故通報單', '13', '/issueForm/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('22', '合約審閱申請單', '13', '/contractReviewForm/init', '0', '1');
-- 選單(表單管理)
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('23', '表單管理', '0', NULL, '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('24', '測試單管理', '23', '/qcFormMgr/init', '0', '1');
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('25', '表單流程管理', '23', '/flowMgr/init', '0', '1');

-- 遊戲資料
DELETE FROM game_data;
ALTER TABLE game_data AUTO_INCREMENT = 1;

INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`, `status`) VALUES ('1', '傾城記', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('2', '上古戰魂--重裝武士', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('3', '魅子Online', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('4', '仙國志', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('5', '崩壞學園', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('6', '君臨天下', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('7', '仙境之戀2', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('8', '岳戰天下', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('9', '坦克隊長', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('10', 'Q爆江湖', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('11', '笑傲江湖', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('12', '大英雄', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('13', '幻想編年史', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('14', '謝丞相', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('15', '彈彈堂', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('16', '劍花煙雨', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('17', '樂舞', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('18', '決戰天龍', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('19', '馴龍戰機', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('20', '聖靈ZERO', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('21', '亂鬥堂2', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('22', '六扇門', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('23', '亂鬥堂2(HK)', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('24', '血色紀元', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('25', '女神100', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('26', '魔域傳奇', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('27', '暗黑黎明2', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('28', '御劍情緣', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('29', '暗黑黎明2(HK)', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('30', '倩女幽魂', '1', '1', '1');
INSERT INTO `game_data` (`id`, `name`, `company_id`, `online`,  `status`) VALUES ('31', '검 그리고 사랑(구글)', '1', '1', '1');

-- 金流資料
DELETE FROM pay_type;
ALTER TABLE pay_type AUTO_INCREMENT = 1;

INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('1', 'mycard-ingame-HK', 'MyCard-點數卡(香港)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('2', 'mycard-ingame-TW', 'MyCard-點數卡(臺灣)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('3', 'mycard-ingame-1', 'MyCard-點數卡(虛擬)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('4', 'mycard-ingame-2', 'MyCard-點數卡(一般通路)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('5', 'mycard-ingame-3', 'MyCard-點數卡(會員)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('6', 'mycard-ingame-5', 'MyCard-點數卡(海外港澳舊價)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('7', 'mycard-ingame-7', 'MyCard-點數卡(海外大馬)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('8', 'mycard-ingame-8', 'MyCard-點數卡(海外港澳新價)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('9', 'mycard-ingame-18', 'MyCard-點數卡(全家超商通路)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('10', 'mycard-ingame-19', 'MyCard-點數卡(全家之外超商通路)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('11', 'mycard-ingame-20', 'MyCard-點數卡(行動遊戲卡)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('12', 'mycard-ingame-32', 'MyCard-點數卡(海外大馬新價)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('13', 'mycard-billing-GTSHWEBATM', 'MyCard-webATM-國泰世華', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('14', 'mycard-billing-TELFET01', 'MyCard-遠傳電信', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('15', 'mycard-point', 'MyCard-電子錢包', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('16', 'mycard-billing-TELCHT03', 'MyCard-中華電信', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('17', 'mycard-billing-TELSON04', 'MyCard-亞太電信', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('18', 'mycard-billing-BNK82201', 'MyCard-台灣地區信用卡付款(3D驗證)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('19', 'mycard-billing-TELCHT01', 'MyCard-中華電信市內電話', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('20', 'mycard-billing-BNK80801', 'MyCard-webATM-玉山銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('21', 'mycard-billing-TELVIBO', 'MyCard-台灣之星(威寶)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('22', 'mycard-billing-TELTCC01', 'MyCard-台灣大哥大', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('23', 'mycard-billing-ZGYZWEBATM', 'MyCard-webATM-中國郵政', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('24', 'mycard-billing-TBFBWEBATM', 'MyCard-webATM-台北富邦', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('25', 'mycard-billing-TXWEBATM', 'MyCard-webATM-台新銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('26', 'mycard-billing-ZGXTWEBATM', 'MyCard-webATM-中國信託', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('27', 'mycard-billing-TWWEBATM', 'MyCard-webATM-台灣銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('28', 'mycard-billing-ZHWEBATM', 'MyCard-webATM-彰化銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('29', 'mycard-billing-HNWEBATM', 'MyCard-webATM-華南銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('30', 'mycard-billing-SHWEBATM', 'MyCard-webATM-上海銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('31', 'mycard-billing-HZJKWEBATM', 'MyCard-webATM-合作金庫', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('32', 'mycard-billing-DYWEBATM', 'MyCard-webATM-第一銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('33', 'mycard-billing-XGWEBATM', 'MyCard-webATM-新光銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('34', 'mycard-billing-ZFWEBATM', 'MyCard-webATM-兆豐銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('35', 'mycard-billing-TDWEBATM', 'MyCard-webATM-土地銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('36', 'mycard-billing-YINLIAN', 'MyCard-銀聯在線支付', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('37', 'mycard-billing-CAIFUTONG', 'MyCard-財付通', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('38', 'mycard-billing-KUAIQIAN', 'MyCard-快錢', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('39', 'mycard-billing-FUYOU', 'MyCard-富友支付', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('40', 'mycard-billing-ALIPAY', 'MyCard-支付寶', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('41', 'mycard-billing-PAYPAL', 'MyCard-PayPal', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('42', 'mycard-billing-RSWEBATM', 'MyCard-webATM-日盛銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('43', 'gash-BNK82201', 'Gash-信用卡', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('44', 'gash-TELTCC01', 'Gash-台灣大哥大', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('45', 'gash-BNK80804', 'Gash-支付寶', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('46', 'gash-TELCHT06', 'Gash-中華電信HiNet', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('47', 'gash-COPPAL01', 'Gash-PayPal', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('48', 'gash-TELFET01', 'Gash-遠傳電信', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('49', 'gash-BNK80801', 'Gash-webATM-玉山銀行', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('50', 'gash-TELCHT07', 'Gash-中華電信市內電話', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('51', 'gash-BNKRBS01', 'Gash-國際信用卡', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('52', 'gash-TELVIBO', 'Gash- 台灣之星(威寶)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('53', 'gash-TELCHT05', 'Gash-中華電信手機', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('54', 'gash-TELSON04', 'Gash-亞太電信', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('55', 'gash-COPGAM05-MYR', 'Gash-馬幣點數卡', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('56', 'gash-COPGAM05-TWD', 'Gash-台灣點數卡(港台)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('57', 'gash-COPGAM05-HKD', 'Gash-香港點數卡(港台)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('58', 'gash-COPGAM03', 'Gash-香港點數卡', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('59', 'gash-COPGAM09', 'Gash-電子錢包', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('60', 'GooglePlay', 'GooglePlay', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('61', 'Appstore', 'Appstore', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('62', 'bank-hk', '大額匯款(香港)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('63', 'bank-tw', '大額匯款(臺灣)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('64', 'hkgame_home', '到府收款(香港)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('65', 'twgame_home', '到府收款(臺灣)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('66', 'ChinaTrust-Credit', '中國信託(臺灣信用卡)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('67', 'ChinaTrust-UnionPay', '中國信託(銀聯卡)', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('68', 'Alipay', 'Alipay', '1');
INSERT INTO `eform`.`pay_type` (`id`, `code`, `name`, `status`) VALUES ('69', 'Paypal', 'Paypal', '1');

-- 角色
DELETE FROM t_erp_role;
ALTER TABLE t_erp_role AUTO_INCREMENT = 1;

INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (1, '管理員', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (2, '總經理', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (3, '副總經理', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (4, '董事長', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (5, '行政主管', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (6, '營運主管', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (7, '技術主管', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (8, '客服主管', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (9, '商務主管', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (10, '行銷主管', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (11, '資訊主管', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (12, '出納', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (13, '總務', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (14, '會計', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (15, '人資', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (16, '產品', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (17, '技術', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (18, '客服', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (19, '測試', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (20, '法務', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (21, '一般', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (22, '韓國辦公室', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (23, '牛番茄', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (24, '鼎客', '1');
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (25, '風靡', '1');

-- 角色功能(系統管理員，不含表單)
DELETE FROM t_erp_role_function;
ALTER TABLE t_erp_role_function AUTO_INCREMENT = 1;

-- 系統管理員，不含表單
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '1', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '2', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '3', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '4', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '5', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '6', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '7', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '8', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '9', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '10', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '11', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '12', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '23', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '24', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '25', '1');

-- 用戶
-- 系統管理員(admin/efun)
DELETE FROM t_erp_user;
ALTER TABLE t_erp_user AUTO_INCREMENT = 1;

INSERT INTO `t_erp_user` (`id`,`login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES (1,'admin', '-1', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);

-- 用戶角色
-- 系統管理員
DELETE FROM t_erp_user_role;
ALTER TABLE t_erp_user_role AUTO_INCREMENT = 1;

INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('1', '1', '1', '1');

-- 表單清單
DELETE FROM form;
ALTER TABLE form AUTO_INCREMENT = 1;

INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('1', '請款單', 'PA', '1', '請款單', 'PA', '/paymentForm/init', '/paymentForm/process', '/paymentForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('2', '公司卡消費通知單', 'BC', '2', '公司卡消費通知單', 'BC', '/businessCardForm/init', '/businessCardForm/process', '/businessCardForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('3', '退款申請單', 'RE', '3', '退款申請單', 'RE', '/refundForm/init', '/refundForm/process', '/refundForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('4', '出差申請單', 'TA', '4', '出差申請單', 'TA', '/businessTripForm/init', '/businessTripForm/process', '/businessTripForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('5', '出差報支單', 'PT', '5', '出差報支單', 'PT', '/businessTripExpenseForm/init', '/businessTripExpenseForm/process', '/businessTripExpenseForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('6', '部門固定成本運算表', 'BU', '8', '部門固定成本運算表', 'BU', '/budgetForm/init', '/budgetForm/process', '/budgetForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('7', '重大事故通報單', 'IS', '6', '重大事故通報單', 'IS', '/issueForm/init', '/issueForm/process', '/issueForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('8', '合約審閱申請單', 'CO', '7', '合約審閱申請單', 'CO', '/contractReviewForm/init', '/contractReviewForm/process', '/contractReviewForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('9', '8playAPP檢核表', '8AT', '10', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('10', 'Android檢核表', 'AST', '10', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('11', 'friDay包檢核表', 'FDT', '9', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('12', 'GameXDD檢核表', 'GXD', '10', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('13', 'Gotoplay檢核表', 'G2P', '10', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('14', 'iOS檢核表', 'IST', '10', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('15', '兌換類活動檢核表', 'ECA', '10', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('16', '官網金流檢核表', 'WBG', '9', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('17', '活動上線前檢核表', 'AOT', '9', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('18', '活動返利檢核表', 'ART', '10', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('19', '新OS推出檢核表', 'OST', '9', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('20', '遊戲上線檢核表', 'ONT', '9', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('21', '遊戲內金流檢核表', 'IGG', '9', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('22', '遊戲合服維護檢核表', 'GCM', '9', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('23', '遊戲維護檢核表', 'GMT', '9', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('24', '預約登入活動檢核表', 'PLT', '10', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('25', '儲值類活動檢核表', 'PAT', '10', '測試單', 'QC', '/qcForm/init', '/qcForm/process', '/qcForm/get');

-- 表單角色
DELETE FROM form_role;
ALTER TABLE form_role AUTO_INCREMENT = 1;

INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (1, '董事長', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (2, '總經理', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (3, '副總經理', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (4, '部門主管', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (5, '行政主管', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (6, '營運主管', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (7, '技術主管', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (8, '客服主管', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (9, '出納', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (10, '總務', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (11, '會計', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (12, '人資', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (13, '產品', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (14, '技術', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (15, '客服', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (16, '測試', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (17, '法務', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (18, '會計專員', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (19, '重大負責人', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (20, '重大修復人', '1');
INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (21, '重大執行人', '1');

-- 表單權限
DELETE FROM form_auth;
ALTER TABLE form_auth AUTO_INCREMENT = 1;

INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('PA', '4', '5');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('PA', '4', '9');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('PA', '4', '11');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('BC', '4', '5');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('BC', '4', '9');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('BC', '4', '11');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('RE', '4', '5');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('TA', '4', '5');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('TA', '4', '9');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('PT', '4', '5');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('PT', '4', '11');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('BU', '4', '2');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('BU', '4', '3');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('BU', '4', '5');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('IS', '4', '2');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('IS', '4', '3');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('IS', '4', '6');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('IS', '4', '8');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('IS', '4', '13');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('IS', '4', '15');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('CO', '4', '2');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('CO', '4', '3');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('CO', '4', '5');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('QC', '4', '6');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('QC', '4', '7');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('QC', '4', '13');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('QC', '4', '16');

-- 流程
DELETE FROM flow_schema;
ALTER TABLE flow_schema AUTO_INCREMENT = 1;
DELETE FROM flow_step;
ALTER TABLE flow_step AUTO_INCREMENT = 1;
DELETE FROM flow_router;
ALTER TABLE flow_router AUTO_INCREMENT = 1;

-- 流程(請款單)
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('1', '請款單', '1', '1', '1', NOW());
-- 流程步驟(請款單)
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`, `router_id`) VALUES ('1', '1', '1', '1', '-1', '1', NULL, '0', '2');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`, `router_id`) VALUES ('2', '1', '3', '2', '4', '1', NULL, '2', '1,4,5,6,7');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('3', '1', '3', '3', '6', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('4', '1', '3', '3', '2', '1', NULL, '2');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('5', '1', '9', '4', '-1', '1', NULL, '0');
-- 流程條件(產品部)
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('1', 'requestDepartment = 8', '3', '產品部(Power)');

-- 流程(公司卡消費通知單)
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('2', '公司卡消費通知單', '1', '1', '1', NOW());
-- 流程步驟(公司卡消費通知單)
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`, `router_id`) VALUES ('6', '2', '1', '1', '-1', '1', NULL, '0', '9');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('7', '2', '3', '2', '4', '1', NULL, '2');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('8', '2', '9', '3', '-1', '1', NULL, '0');

-- 流程(退款單)
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('3', '退款單', '1', '1', '1', NOW());
-- 流程步驟(退款單)
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('9', '3', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('10', '3', '2', '2', '8', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('11', '3', '2', '3', '13', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('12', '3', '2', '4', '6', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('13', '3', '2', '5', '9', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`, `router_id`) VALUES ('14', '3', '3', '6', '5', '1', NULL, '0', '3');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('15', '3', '2', '7', '18', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('16', '3', '3', '8', '11', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('17', '3', '9', '9', '-1', '1', NULL, '0');
-- 流程條件(退款單)
-- 無財務退款直接結束
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('3', 'finProcess = 5', '15', '退款');

-- 出差申請單
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('4', '出差申請單', '1', '1', '1', NOW());
-- 流程步驟(出差申請單)
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`, `router_id`) VALUES ('18', '4', '1', '1', '-1', '1', NULL, '0', '10');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('19', '4', '2', '2', '4', '1', NULL, '2');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('20', '4', '3', '3', '10', '2', '12,11', '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('21', '4', '9', '4', '-1', '1', NULL, '0');

-- 流程(出差報支單)
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('5', '出差報支單', '1', '1', '1', NOW());
-- 流程步驟(出差報支單)
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`, `router_id`) VALUES ('22', '5', '1', '1', '-1', '1', NULL, '0', '11');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('23', '5', '2', '2', '4', '1', NULL, '2');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('24', '5', '3', '3', '9', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('25', '5', '9', '4', '-1', '1', NULL, '0');

-- 流程(重大事故通報單)
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('6', '重大事故通報單', '1', '1', '1', NOW());
-- 流程步驟(重大事故通報單)
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('26', '6', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('27', '6', '2', '2', '19', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('28', '6', '2', '3', '20', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('29', '6', '2', '4', '21', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('30', '6', '2', '5', '6', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('31', '6', '2', '6', '7', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('32', '6', '2', '7', '3', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('33', '6', '3', '8', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('34', '6', '9', '9', '-1', '1', NULL, '0');

-- 流程(合約審閱申請單)
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('7', '合約審閱申請單', '1', '1', '1', NOW());
-- 流程步驟(合約審閱申請單)
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`, `router_id`) VALUES ('35', '7', '1', '1', '-1', '1', NULL, '0', '12');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('36', '7', '2', '2', '4', '1', NULL, '2');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('37', '7', '2', '3', '5', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('38', '7', '2', '4', '17', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('39', '7', '2', '5', '3', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('40', '7', '3', '6', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('41', '7', '9', '7', '-1', '1', NULL, '0');

-- 流程(部門固定成本預算表)
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('8', '部門固定成本預算表', '1', '1', '1', NOW());
-- 流程步驟(部門固定成本預算表)
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`, `router_id`) VALUES ('42', '8', '1', '1', '-1', '1', NULL, '0', '13');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('43', '8', '2', '2', '4', '2', '11', '2');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('44', '8', '2', '3', '3', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('45', '8', '3', '4', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('46', '8', '9', '5', '-1', '1', NULL, '0');

-- 流程(檢核表一)
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('9', '檢核表(一)', '1', '1', '1', NOW());
-- 流程步驟(檢核表一)
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('47', '9', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('48', '9', '2', '2', '7', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('49', '9', '2', '3', '13', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('50', '9', '3', '4', '6', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('51', '9', '9', '5', '-1', '1', NULL, '0');

-- 流程(檢核表二)
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('10', '檢核表(二)', '1', '1', '1', NOW());
-- 流程步驟(檢核表二)
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('52', '10', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('53', '10', '2', '2', '7', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('54', '10', '2', '3', '13', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('55', '10', '2', '4', '6', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('56', '10', '2', '5', '16', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('57', '10', '2', '6', '7', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('58', '10', '2', '7', '13', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('59', '10', '3', '8', '6', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('60', '10', '9', '9', '-1', '1', NULL, '0');

-- 樂服
-- 請款單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('26', '請款單', 'PA', '11', '請款單', 'PA', '/paymentForm/init', '/paymentForm/process', '/paymentForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('11', '請款單', '1', '3', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('61', '11', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('62', '11', '2', '2', '5', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('63', '11', '3', '3', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('64', '11', '9', '4', '-1', '1', NULL, '0');

-- 出差申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('27', '出差申請單', 'TA', '12', '出差申請單', 'TA', '/businessTripForm/init', '/businessTripForm/process', '/businessTripForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('12', '出差申請單', '1', '3', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('65', '12', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('66', '12', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('67', '12', '9', '3', '-1', '1', NULL, '0');

-- 出差報支單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('28', '出差報支單', 'PT', '13', '出差報支單', 'PT', '/businessTripExpenseForm/init', '/businessTripExpenseForm/process', '/businessTripExpenseForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('13', '出差報支單', '1', '3', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('68', '13', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('69', '13', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('70', '13', '9', '3', '-1', '1', NULL, '0');

-- 合約審閱申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('29', '合約審閱申請單', 'CO', '14', '合約審閱申請單', 'CO', '/contractReviewForm/init', '/contractReviewForm/process', '/contractReviewForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('14', '合約審閱申請單', '1', '3', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('71', '14', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('72', '14', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('73', '14', '9', '3', '-1', '1', NULL, '0');

-- 部門固定成本預算表
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('30', '部門固定成本運算表', 'BU', '15', '部門固定成本運算表', 'BU', '/budgetForm/init', '/budgetForm/process', '/budgetForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('15', '部門固定成本運算表', '1', '3', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('74', '15', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('75', '15', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('76', '15', '9', '3', '-1', '1', NULL, '0');

-- 伊凡達科技(香港)
-- 請款單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('31', '請款單', 'PA', '16', '請款單', 'PA', '/paymentForm/init', '/paymentForm/process', '/paymentForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('16', '請款單', '1', '8', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('77', '16', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('78', '16', '3', '2', '5', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('79', '16', '9', '3', '-1', '1', NULL, '0');

-- 出差申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('32', '出差申請單', 'TA', '17', '出差申請單', 'TA', '/businessTripForm/init', '/businessTripForm/process', '/businessTripForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('17', '出差申請單', '1', '8', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('80', '17', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('81', '17', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('82', '17', '9', '3', '-1', '1', NULL, '0');

-- 出差報支單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('33', '出差報支單', 'PT', '18', '出差報支單', 'PT', '/businessTripExpenseForm/init', '/businessTripExpenseForm/process', '/businessTripExpenseForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('18', '出差報支單', '1', '8', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('83', '18', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('84', '18', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('85', '18', '9', '3', '-1', '1', NULL, '0');

-- 合約審閱申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('34', '合約審閱申請單', 'CO', '19', '合約審閱申請單', 'CO', '/contractReviewForm/init', '/contractReviewForm/process', '/contractReviewForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('19', '合約審閱申請單', '1', '8', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('86', '19', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('87', '19', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('88', '19', '9', '3', '-1', '1', NULL, '0');

-- 部門固定成本預算表
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('35', '部門固定成本運算表', 'BU', '20', '部門固定成本運算表', 'BU', '/budgetForm/init', '/budgetForm/process', '/budgetForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('20', '部門固定成本運算表', '1', '8', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('89', '20', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('90', '20', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('91', '20', '9', '3', '-1', '1', NULL, '0');

-- 牛番茄TW
-- 請款單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('36', '請款單', 'PA', '21', '請款單', 'PA', '/paymentForm/init', '/paymentForm/process', '/paymentForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('21', '請款單', '1', '6', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('92', '21', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('93', '21', '2', '2', '5', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('94', '21', '3', '3', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('95', '21', '9', '4', '-1', '1', NULL, '0');

-- 出差申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('37', '出差申請單', 'TA', '22', '出差申請單', 'TA', '/businessTripForm/init', '/businessTripForm/process', '/businessTripForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('22', '出差申請單', '1', '6', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('96', '22', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('97', '22', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('98', '22', '9', '3', '-1', '1', NULL, '0');

-- 出差報支單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('38', '出差報支單', 'PT', '23', '出差報支單', 'PT', '/businessTripExpenseForm/init', '/businessTripExpenseForm/process', '/businessTripExpenseForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('23', '出差報支單', '1', '6', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('99', '23', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('100', '23', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('101', '23', '9', '3', '-1', '1', NULL, '0');

-- 合約審閱申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('39', '合約審閱申請單', 'CO', '24', '合約審閱申請單', 'CO', '/contractReviewForm/init', '/contractReviewForm/process', '/contractReviewForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('24', '合約審閱申請單', '1', '6', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('102', '24', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('103', '24', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('104', '24', '9', '3', '-1', '1', NULL, '0');

-- 部門固定成本預算表
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('40', '部門固定成本運算表', 'BU', '25', '部門固定成本運算表', 'BU', '/budgetForm/init', '/budgetForm/process', '/budgetForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('25', '部門固定成本運算表', '1', '6', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('105', '25', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('106', '25', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('107', '25', '9', '3', '-1', '1', NULL, '0');

-- 傾國
-- 請款單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('41', '請款單', 'PA', '26', '請款單', 'PA', '/paymentForm/init', '/paymentForm/process', '/paymentForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('26', '請款單', '1', '10', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('108', '26', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('109', '26', '3', '2', '5', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('110', '26', '9', '3', '-1', '1', NULL, '0');

-- 出差申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('42', '出差申請單', 'TA', '27', '出差申請單', 'TA', '/businessTripForm/init', '/businessTripForm/process', '/businessTripForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('27', '出差申請單', '1', '10', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('111', '27', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('112', '27', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('113', '27', '9', '3', '-1', '1', NULL, '0');

-- 出差報支單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('43', '出差報支單', 'PT', '28', '出差報支單', 'PT', '/businessTripExpenseForm/init', '/businessTripExpenseForm/process', '/businessTripExpenseForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('28', '出差報支單', '1', '10', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('114', '28', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('115', '28', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('116', '28', '9', '3', '-1', '1', NULL, '0');

-- 合約審閱申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('44', '合約審閱申請單', 'CO', '29', '合約審閱申請單', 'CO', '/contractReviewForm/init', '/contractReviewForm/process', '/contractReviewForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('29', '合約審閱申請單', '1', '10', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('117', '29', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('118', '29', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('119', '29', '9', '3', '-1', '1', NULL, '0');

-- 部門固定成本預算表
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('45', '部門固定成本運算表', 'BU', '30', '部門固定成本運算表', 'BU', '/budgetForm/init', '/budgetForm/process', '/budgetForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('30', '部門固定成本運算表', '1', '10', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('120', '30', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('121', '30', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('122', '30', '9', '3', '-1', '1', NULL, '0');

-- 鼎客
-- 請款單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('46', '請款單', 'PA', '31', '請款單', 'PA', '/paymentForm/init', '/paymentForm/process', '/paymentForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('31', '請款單', '1', '9', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('123', '31', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('124', '31', '3', '2', '5', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('125', '31', '9', '3', '-1', '1', NULL, '0');

-- 出差申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('47', '出差申請單', 'TA', '32', '出差申請單', 'TA', '/businessTripForm/init', '/businessTripForm/process', '/businessTripForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('32', '出差申請單', '1', '9', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('126', '32', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('127', '32', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('128', '32', '9', '3', '-1', '1', NULL, '0');

-- 出差報支單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('48', '出差報支單', 'PT', '33', '出差報支單', 'PT', '/businessTripExpenseForm/init', '/businessTripExpenseForm/process', '/businessTripExpenseForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('33', '出差報支單', '1', '9', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('129', '33', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('130', '33', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('131', '33', '9', '3', '-1', '1', NULL, '0');

-- 合約審閱申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('49', '合約審閱申請單', 'CO', '34', '合約審閱申請單', 'CO', '/contractReviewForm/init', '/contractReviewForm/process', '/contractReviewForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('34', '合約審閱申請單', '1', '9', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('132', '34', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('133', '34', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('134', '34', '9', '3', '-1', '1', NULL, '0');

-- 部門固定成本預算表
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('50', '部門固定成本運算表', 'BU', '35', '部門固定成本運算表', 'BU', '/budgetForm/init', '/budgetForm/process', '/budgetForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('35', '部門固定成本運算表', '1', '9', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('135', '35', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('136', '35', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('137', '35', '9', '3', '-1', '1', NULL, '0');

-- 風靡
-- 請款單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('51', '請款單', 'PA', '36', '請款單', 'PA', '/paymentForm/init', '/paymentForm/process', '/paymentForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('36', '請款單', '1', '2', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('138', '36', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('139', '36', '3', '2', '5', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('140', '36', '9', '3', '-1', '1', NULL, '0');

-- 出差申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('52', '出差申請單', 'TA', '37', '出差申請單', 'TA', '/businessTripForm/init', '/businessTripForm/process', '/businessTripForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('37', '出差申請單', '1', '2', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('141', '37', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('142', '37', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('143', '37', '9', '3', '-1', '1', NULL, '0');

-- 出差報支單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('53', '出差報支單', 'PT', '38', '出差報支單', 'PT', '/businessTripExpenseForm/init', '/businessTripExpenseForm/process', '/businessTripExpenseForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('38', '出差報支單', '1', '2', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('144', '38', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('145', '38', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('146', '38', '9', '3', '-1', '1', NULL, '0');

-- 合約審閱申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('54', '合約審閱申請單', 'CO', '39', '合約審閱申請單', 'CO', '/contractReviewForm/init', '/contractReviewForm/process', '/contractReviewForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('39', '合約審閱申請單', '1', '2', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('147', '39', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('148', '39', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('149', '39', '9', '3', '-1', '1', NULL, '0');

-- 部門固定成本預算表
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('55', '部門固定成本運算表', 'BU', '40', '部門固定成本運算表', 'BU', '/budgetForm/init', '/budgetForm/process', '/budgetForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('40', '部門固定成本運算表', '1', '2', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('150', '40', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('151', '40', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('152', '40', '9', '3', '-1', '1', NULL, '0');

-- 牛番茄HK
-- 請款單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('56', '請款單', 'PA', '41', '請款單', 'PA', '/paymentForm/init', '/paymentForm/process', '/paymentForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('41', '請款單', '1', '5', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('153', '41', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('154', '41', '2', '2', '5', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('155', '41', '3', '3', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('156', '41', '9', '4', '-1', '1', NULL, '0');

-- 出差申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('57', '出差申請單', 'TA', '42', '出差申請單', 'TA', '/businessTripForm/init', '/businessTripForm/process', '/businessTripForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('42', '出差申請單', '1', '5', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('157', '42', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('158', '42', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('159', '42', '9', '3', '-1', '1', NULL, '0');

-- 出差報支單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('58', '出差報支單', 'PT', '43', '出差報支單', 'PT', '/businessTripExpenseForm/init', '/businessTripExpenseForm/process', '/businessTripExpenseForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('43', '出差報支單', '1', '5', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('160', '43', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('161', '43', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('162', '43', '9', '3', '-1', '1', NULL, '0');

-- 合約審閱申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('59', '合約審閱申請單', 'CO', '44', '合約審閱申請單', 'CO', '/contractReviewForm/init', '/contractReviewForm/process', '/contractReviewForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('44', '合約審閱申請單', '1', '5', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('163', '59', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('164', '59', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('165', '59', '9', '3', '-1', '1', NULL, '0');

-- 部門固定成本預算表
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('60', '部門固定成本運算表', 'BU', '45', '部門固定成本運算表', 'BU', '/budgetForm/init', '/budgetForm/process', '/budgetForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('45', '部門固定成本運算表', '1', '5', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('166', '60', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('167', '60', '3', '2', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('168', '60', '9', '3', '-1', '1', NULL, '0');

-- 香港商伊凡達
-- 請款單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('61', '請款單', 'PA', '46', '請款單', 'PA', '/paymentForm/init', '/paymentForm/process', '/paymentForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('46', '請款單', '1', '7', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('169', '46', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('170', '46', '3', '2', '5', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('171', '46', '9', '3', '-1', '1', NULL, '0');

-- 出差申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('62', '出差申請單', 'TA', '47', '出差申請單', 'TA', '/businessTripForm/init', '/businessTripForm/process', '/businessTripForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('47', '出差申請單', '1', '7', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('172', '47', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('173', '47', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('174', '47', '9', '3', '-1', '1', NULL, '0');

-- 出差報支單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('63', '出差報支單', 'PT', '48', '出差報支單', 'PT', '/businessTripExpenseForm/init', '/businessTripExpenseForm/process', '/businessTripExpenseForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('48', '出差報支單', '1', '7', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('175', '48', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('176', '48', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('177', '48', '9', '3', '-1', '1', NULL, '0');

-- 合約審閱申請單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('64', '合約審閱申請單', 'CO', '49', '合約審閱申請單', 'CO', '/contractReviewForm/init', '/contractReviewForm/process', '/contractReviewForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('49', '合約審閱申請單', '1', '7', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('178', '49', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('179', '49', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('180', '49', '9', '3', '-1', '1', NULL, '0');

-- 部門固定成本預算表
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('65', '部門固定成本運算表', 'BU', '50', '部門固定成本運算表', 'BU', '/budgetForm/init', '/budgetForm/process', '/budgetForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('50', '部門固定成本運算表', '1', '7', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('181', '50', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('182', '50', '3', '2', '4', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('183', '50', '9', '3', '-1', '1', NULL, '0');

-- 清除表單資料
DELETE FROM work_flow;
ALTER TABLE work_flow AUTO_INCREMENT = 1;

DELETE FROM work_flow_detail;
ALTER TABLE work_flow_detail AUTO_INCREMENT = 1;

DELETE FROM payment_form;
ALTER TABLE payment_form AUTO_INCREMENT = 1;

DELETE FROM business_card_form;
ALTER TABLE business_card_form AUTO_INCREMENT = 1;

DELETE FROM refund_form;
ALTER TABLE refund_form AUTO_INCREMENT = 1;

DELETE FROM business_trip_form;
ALTER TABLE business_trip_form AUTO_INCREMENT = 1;

DELETE FROM business_trip_expense_form;
ALTER TABLE business_trip_expense_form AUTO_INCREMENT = 1;

DELETE FROM budget_form;
ALTER TABLE budget_form AUTO_INCREMENT = 1;

DELETE FROM issue_form;
ALTER TABLE issue_form AUTO_INCREMENT = 1;

DELETE FROM issue_memo;
ALTER TABLE issue_memo AUTO_INCREMENT = 1;

DELETE FROM contract_review_form;
ALTER TABLE contract_review_form AUTO_INCREMENT = 1;

DELETE FROM qc_form;
ALTER TABLE qc_form AUTO_INCREMENT = 1;

DELETE FROM receipt;
ALTER TABLE receipt AUTO_INCREMENT = 1;

DELETE FROM attachment;
ALTER TABLE attachment AUTO_INCREMENT = 1;

DELETE FROM sequence_data;
