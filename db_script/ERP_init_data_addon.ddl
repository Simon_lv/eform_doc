-- 請款單
-- 財會
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('184', '1', '3', '3', '2', '1', NULL, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('4', 'requestDepartment = 2', '184', '總管理處(財會)');

-- 電商
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('185', '1', '3', '3', '2', '1', NULL, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('5', 'requestDepartment = 14', '185', '總管理處(電商)');

-- 行政
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('186', '1', '3', '3', '2', '1', NULL, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('6', 'requestDepartment = 11', '186', '總管理處(行政)');

-- 儲訓
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('187', '1', '3', '3', '2', '1', NULL, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('7', 'requestDepartment = 6', '187', '總管理處(儲訓)');
 
-- 總管理處
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`, `router_id`) VALUES ('188', '1', '3', '2', '3', '1', NULL, '0', '8');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('2', 'requestDepartment = 1', '188', '總管理處(請款)一放');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('8', 'requestDepartment = 1', '4', '總管理處(請款)二放');

-- 公司卡消費通知單
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('189', '2', '3', '2', '3', '1', NULL, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('9', 'requestDepartment = 1', '189', '總管理處(公司卡)');

-- 出差申請單
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('190', '4', '2', '2', '3', '1', NULL, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('10', 'requestDepartment = 1', '190', '總管理處(出差申請)');

-- 出差報支單
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('191', '5', '2', '2', '3', '1', NULL, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('11', 'requestDepartment = 1', '191', '總管理處(出差報支)');

-- 合約審閱申請單
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('192', '7', '2', '2', '3', '1', NULL, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('12', 'requestDepartment = 1', '192', '總管理處(合約)');

-- 部門固定成本運算表
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('193', '8', '2', '2', '3', '2', 11, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('13', 'requestDepartment = 1', '193', '總管理處(部門成本)');

-- 遊戲
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('新古龍群俠傳', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('球球大作戰', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('倩女幽魂(HK)', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('六扇門(HK)', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('守護者之森', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('守護者之森-體驗包', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('守護者之森(HK)-體驗包', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('聖靈衝擊', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('聖靈衝擊(HK)', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('War Storm', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('War Storm(HK)', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('戀愛100', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('戀愛100(HK)', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('名將爭霸', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('樂舞(HK)', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('決戰天龍', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('三國齊打交', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('崩壞學園2', '1', '1', '1');
INSERT INTO `eform`.`game_data` (`name`, `company_id`, `online`, `status`) VALUES ('岳戰天下(星馬版)', '1', '1', '1');

-- 角色
INSERT INTO `t_erp_role` (`id`, `name`, `status`) VALUES (26, '特助', '1');
-- 特助 angelina
UPDATE t_erp_user_role SET role_id=26 WHERE user_id=23;

INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '6', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '7', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '8', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '9', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '10', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '17', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '21', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('26', '22', '1');

INSERT INTO `form_role` (`id`, `name`, `status`) VALUES (22, '特助', '1');
INSERT INTO `eform`.`form_user_role` (`user_id`, `role_id`, `same_department`, `status`) VALUES ('23', '22', '1', '1');

INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('PA', '4', '22');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('BC', '4', '22');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('RE', '4', '22');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('TA', '4', '22');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('PT', '4', '22');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('BU', '4', '22');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('IS', '4', '22');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('CO', '4', '22');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('QC', '4', '22');

-- 退款單查詢(客服)
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('RE', '4', '8');
INSERT INTO `form_auth` (`form_type`, `authority`, `form_role`) VALUES ('RE', '4', '15');

-- 步驟條件欄位長度
ALTER TABLE `flow_step`
MODIFY COLUMN `router_id`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'flow_router.id' AFTER `same_department`;

-- 出差申請
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('194', '4', '2', '2', '2', '1', NULL, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('14', 'requestDepartment = 2,6,11,14', '194', '總管理處各組');

UPDATE `flow_step` SET `router_id`='14' WHERE `id`=19;

INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('17', 'requestDepartment = 1', '194', '總管理處');
UPDATE `flow_step` SET `router_id`='17' WHERE `id`=190;

-- 出差報支
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('195', '5', '2', '2', '2', '1', NULL, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('15', 'requestDepartment = 2,6,11,14', '195', '總管理處各組');

UPDATE `flow_step` SET `router_id`='15' WHERE `id`=23;

INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('18', 'requestDepartment = 1', '195', '總管理處');
UPDATE `flow_step` SET `router_id`='18' WHERE `id`=191;

-- 公司卡
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('196', '2', '3', '3', '2', '1', NULL, '0');
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('16', 'requestDepartment = 2,6,11,14', '196', '總管理處各組');

INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('19', 'requestDepartment = 1', '196', '總管理處');
UPDATE `flow_step` SET `router_id`='19' WHERE `id`=189;

UPDATE `flow_step` SET `router_id`='16' WHERE `id`=7;
UPDATE `flow_step` SET `step_order`='4' WHERE `id`=8;

-- 請款單大額
INSERT INTO `flow_router` (`id`, `condition`, `next_step`, `description`) VALUES ('20', 'totalAmount >= 1000000', '4', '大額請款');
UPDATE `flow_step` SET `router_id`='20' WHERE `id`=3;
UPDATE `flow_step` SET `router_id`='1,4,5,6,7,20' WHERE `id`=2;
UPDATE `flow_step` SET `same_department`='0' WHERE `id`=4;

-- 福委會
INSERT INTO `eform`.`company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('11', '伊凡達科技股份有限公司職工福利委員會', '54140696', '臺北市松山區光復南路35號4樓之1', '02-27470696', '劉孝椽', '2017-08-24', NULL, NULL, '1', NOW(), '1');
-- 請款單
-- 表單清單
INSERT INTO `form` (`id`, `name`, `code`, `flow_schema`, `group_name`, `group_code`, `init_page`, `process_page`, `view_page`) VALUES ('66', '請款單', 'PA', '51', '請款單', 'PA', '/paymentForm/init', '/paymentForm/process', '/paymentForm/get');
-- 流程
INSERT INTO `flow_schema` (`id`, `flow_name`, `flow_type`, `company_id`, `creator`, `create_time`) VALUES ('51', '請款單', '1', '11', '1', NOW());
-- 流程步驟
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('197', '51', '1', '1', '-1', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('198', '51', '2', '2', '5', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('199', '51', '3', '3', '2', '1', NULL, '0');
INSERT INTO `flow_step` (`id`, `flow_schema_id`, `step_type`, `step_order`, `role_id`, `iteration`, `countersign`, `same_department`) VALUES ('200', '51', '9', '4', '-1', '1', NULL, '0');

-- 表單查詢(admin)
INSERT INTO `t_erp_function` (`id`, `name`, `parent_id`, `url`, `type`, `status`) VALUES ('26', '表單狀態查詢', '23', '/formMgr/init', '0', '1');
INSERT INTO `t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('1', '26', '1');

