DELIMITER $$
 
CREATE FUNCTION `nextval` (`seq_name` varchar(10))
RETURNS varchar(20) NOT DETERMINISTIC
BEGIN
    DECLARE cur_val int(11);
		DECLARE seq_date varchar(8);
		SET seq_date = DATE_FORMAT(NOW(), '%Y%m%d');
 
    SELECT
        sequence_cur_value INTO cur_val
    FROM
        sequence_data
    WHERE
        sequence_name = seq_name and sequence_date = seq_date;
 
    IF cur_val IS NOT NULL THEN
        UPDATE
            sequence_data
        SET
            sequence_cur_value = IF (
                (sequence_cur_value + sequence_increment) > sequence_max_value,
                IF (
                    sequence_cycle = TRUE,
                    sequence_min_value,
                    NULL
                ),
                sequence_cur_value + sequence_increment
            )
        WHERE
            sequence_name = seq_name and sequence_date = seq_date;    
		ELSE
				INSERT INTO 
						sequence_data(sequence_name, sequence_date, sequence_cur_value)
				VALUE
						(seq_name, seq_date, 2);
				SET cur_val = 1;
		END IF;		
 
    RETURN CONCAT(seq_name, seq_date, LPAD(cur_val, 2, '0'));
END$$