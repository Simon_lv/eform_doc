-- 系統角色
-- cs/客服
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('2', '12', '15', '1');
-- eric/技術
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('3', '2', '14', '1');
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('31', '22', '14', '1');
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('32', '23', '14', '1');
-- oliva/技術主管,部門主管
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('4', '3', '7', '1');
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('12', '3', '4', '1');
-- arden/董事長
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('5', '4', '26', '1');
-- angel/行政主管,部門主管,行政專員
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('6', '5', '5', '1');
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('14', '5', '4', '1');
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('19', '5', '17', '1');
-- ethan/副總經理
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('7', '6', '3', '1');
-- apple/會計,會計專員
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('8', '7', '11', '1');
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('26', '7', '18', '1');
-- terissa/總務
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('9', '8', '10', '1');
-- nancy/人資
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('10', '9', '12', '1');
-- emma/出納
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('11', '10', '9', '1');
-- tina/測試
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('13', '11', '16', '1');
-- pm/產品
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('15', '13', '13', '1');
-- jay/營運主管
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('16', '14', '6', '1');
-- allen/客服主管
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('17', '15', '8', '1');
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('18', '15', '4', '1');
-- 重大負責人(PW+RD)
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('21', '2', '19', '1');
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('22', '13', '19', '1');
-- 重大修復人(RD)
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('23', '2', '20', '1');
-- 重大執行人(RD)
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('24', '2', '21', '1');
-- law/法務
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('25', '16', '22', '1');
-- amy/會計專員
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('27', '17', '11', '1');
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('20', '17', '18', '1');
-- dolly/行政專員
-- INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('19', '18', '17', '1');
-- 商務主管
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('28', '19', '23', '1');
-- 行銷主管
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('29', '20', '24', '1');
-- 資訊主管
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('30', '21', '25', '1');
-- iris/總經理
INSERT INTO `t_erp_user_role` (`id`, `user_id`, `role_id`, `status`) VALUES ('33', '24', '2', '1');

-- TODO 公司(測試用)
INSERT INTO `company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `update_time`, `modifier`, `create_time`, `creator`, `status`) VALUES ('1', '伊凡達科技股份有限公司', '54140696', '台北市光復南路35巷4F', '02-22124596', '劉孝椽', '2013-12-19', NULL, NULL, NOW(), '1', '1');
INSERT INTO `company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `update_time`, `modifier`, `create_time`, `creator`, `status`) VALUES ('2', '風靡資訊股份有限公司', '43846035', '臺北市松山區光復南路35號4樓之1', '02-77181681', '蕭伊婷', '2016-05-25', NULL, NULL, NOW(), '1', '1');
INSERT INTO `company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `update_time`, `modifier`, `create_time`, `creator`, `status`) VALUES ('3', '樂服股份有限公司', '43836698', '臺北市松山區光復南路35號4樓之1', '02-77181681', '劉孝椽', '2016-05-20', NULL, NULL, NOW(), '1', '1');

-- TODO 部門(伊凡達)(測試用)
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('1', 'A11', '總管理處', '1', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('2', 'C31', '行政部', '3', '1', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('3', 'C32', '研發部', '3', '1', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('4', 'C33', '行銷部', '3', '1', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('5', 'C34', '客服部', '3', '1', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('6', 'C35', '運維部', '3', '1', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('7', 'C36', '產品部(Magic)', '3', '1', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('8', 'C37', '商務部', '3', '1', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('9', 'C38', '產品部(Secret)', '3', '1', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('10', 'C39', '產品部(Power)', '3', '1', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('11', 'C40', '儲訓部', '3', '1', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('12', 'C41', '新竹研發部', '3', '1', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('13', 'C42', '研發部(廣州)', '3', '23', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('14', 'C43', '儲訓部(廣州)', '3', '23', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('15', 'C44', '產品部(Magic)(廣州)', '3', '23', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('16', 'A12', '總管理處(香港)', '1', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('17', 'C45', '研發部(香港)', '3', '16', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('18', 'C46', '行政部(香港)', '3', '16', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('19', 'C47', '行銷部(香港)', '3', '16', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('20', 'C48', '產品部(Magic)(香港)', '3', '16', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('21', 'C49', '商務部(香港)', '3', '16', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('22', 'C50', '儲訓部(香港)', '3', '16', NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `department` (`id`, `dept_no`, `name`, `dept_level`, `parent_id`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('23', 'A13', '總管理處(廣州)', '1', NULL, NULL, NULL, '1', NOW(), '1', '1');

-- TODO 職稱(伊凡達)(測試用)
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D01', '(約聘)資深專案經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D02', 'Android工程師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D03', 'Android高級工程師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D04', 'Cocos2d-x工程師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D05', 'IOS高級工程師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D06', 'Java工程師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D07', 'Java資深工程師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D08', 'VIP專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D09', 'VIP組長', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D10', '人資經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D11', '工程師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D12', '主辦會計經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D13', '出納助理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D14', '早班-客服專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D15', '早班-客服組長', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D16', '行政專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D17', '行銷助理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D18', '行銷專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D19', '行銷經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D20', '行銷總監', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D21', '技術總監', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D22', '系統架構師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D23', '法務專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D24', '研發助理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D25', '財務長', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D26', '高級工程師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D27', '副總經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D28', '商務總監', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D29', '採購/總務經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D30', '晚班-客服組長', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D31', '晚班-客服副組長', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D32', '晚班-客服專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D33', '產品助理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D34', '產品專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D35', '產品經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D36', '產品總監', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D37', '測試工程師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D38', '董事長兼任總經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D39', '資深客服經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D40', '資深產品經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D41', '資深經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D42', '資深網頁設計專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D43', '遊戲製作人', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D44', '實習工程師(工讀生)', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D45', '網頁設計專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D46', '稽核經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D47', '總機行政專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D48', '顧問', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D49', '商務專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D50', '資深行銷經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D51', '商務法務專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D52', '助理工程師(工讀生)', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D53', '軟體工程師(實習生)', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D54', '經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D55', '會計助理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D56', '測試工讀生(勞務承攬)', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D57', '資深會計經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D58', '總經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D59', '會計經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D60', '按摩師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D61', '講師', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D62', '遊戲評測專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D63', '(副總)行政助理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D64', '數值策畫經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D65', '行政管理總監', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D66', '早班-客服副組長', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D67', '會計/專案經理', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D68', '出納專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D69', '(韓國)行銷專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D70', '駐點教練', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D71', '會計專員', NULL, NULL, NULL, '1', NOW(), '1', '1');
INSERT INTO `job_title` (`title_no`, `name`, `level`, `modifier`, `update_time`, `creator`, `create_time`, `status`, `company_id`) VALUES ('D72', '晚班-客服人員', NULL, NULL, NULL, '1', NOW(), '1', '1');

-- TODO 員工(測試用)
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('1', '1040709', '李治忠', '0912-345-678', '', '', NULL, '6', '3', '1', '2015-07-13', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('2', '1030801', '游怡芳', '0912-345-678', '', '', NULL, '21', '3', '1', '2014-08-06', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('3', '1030402', '劉孝椽', '0912-345-678', '', '', NULL, '38', '1', '1', '2014-04-16', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('4', '1030403', '陳湘涵', '0912-345-678', '', '', NULL, '65', '2', '1', '2014-04-16', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('5', '1031003', '葉孟穎', '0912-345-678', '', '', NULL, '27', '1', '1', '2014-10-28', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('6', '1031102', '洪瑞慈', '0912-345-678', '', '', NULL, '57', '2', '1', '2014-11-04', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('7', '1031105', '游怡雲', '0912-345-678', '', '', NULL, '29', '2', '1', '2014-11-04', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('8', '1031106', '黃秋蓉', '0912-345-678', '', '', NULL, '10', '2', '1', '2014-11-04', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('9', '1040605', '林素如', '0912-345-678', '', '', NULL, '68', '2', '1', '2015-06-10', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('10', '1040206', '王映婷', '0912-345-678', '', '', NULL, '37', '3', '1', '2015-02-24', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('11', '0000001', '客服專員A', '0912-345-678', '', '', NULL, '14', '5', '1', '2015-02-24', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('12', '0000002', '專案經理A', '0912-345-678', '', '', NULL, '14', '5', '1', '2015-02-24', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('13', '1040305', '陳百傑', '0912-345-678', '', '', NULL, '14', '5', '1', '2015-02-24', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('14', '1040307', '蕭少盈', '0912-345-678', '', '', NULL, '14', '5', '1', '2015-02-24', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('15', '0000003', '法務專員A', '0912-345-678', '', '', NULL, '23', '2', '1', '2015-02-24', NULL, NULL, NULL, '1', NOW(), '1');
-- INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('16', '1040512', '王瀅琇', '0912-345-678', '', '', NULL, '16', '2', '1', '2015-05-25', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('17', '1040503', '李佩臻', '0912-345-678', '', '', NULL, '67', '2', '1', '2015-02-24', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('18', '1040309', '劉鎧瑜', '0912-345-678', '', '', NULL, '28', '8', '1', '2015-03-23', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('19', '1031008', '王基宗', '0912-345-678', '', '', NULL, '20', '4', '1', '2014-10-28', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('20', '1050706', '郭信義', '0912-345-678', '', '', NULL, '22', '6', '1', '2015-12-07', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('21', '1031002', '陳國宏', '0912-345-678', '', '', NULL, '41', '3', '1', '2014-10-16', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('22', '1050703', '陳建劭', '0912-345-678', '', '', NULL, '6', '3', '1', '2016-07-04', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `employee` (`id`, `employee_no`, `name`, `mobile`, `phone`, `address`, `phone_ext`, `job_title_id`, `department_id`, `company_id`, `onboard_time`, `quit_time`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('23', '1030401', '蕭伊婷', '0912-345-678', '', '', NULL, '58', '1', '1', '2014-04-16', NULL, NULL, NULL, '1', NOW(), '1');

-- 系統用戶(密碼都是 efun)(測試用)
-- 技術
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('2', 'eric', '1', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('22', 'mike', '21', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('23', 'simon', '22', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 技術主管
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('3', 'oliva', '2', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 董事長
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('4', 'arden', '3', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 行政主管
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('5', 'angel', '4', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 副總經理
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('6', 'ethan', '5', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 會計
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('7', 'apple', '6', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 總務
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('8', 'terissa', '7', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 人資
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('9', 'nancy', '8', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 出納
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('10', 'emma', '9', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 測試
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('11', 'tina', '10', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 客服
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('12', 'cs', '11', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 產品
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('13', 'pm', '12', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 營運主管
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('14', 'jay', '13', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 客服主管
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('15', 'allen', '14', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 法務
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('16', 'law', '15', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 會計
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('17', 'amy', '17', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 行政專員
-- INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('18', 'dolly', '16', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 商務主管
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('19', 'albee', '18', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 行銷主管
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('20', 'red', '19', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 資訊主管
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('21', 'sam', '20', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);
-- 總經理
INSERT INTO `t_erp_user` (`id`, `login_account`, `employee_id`, `status`, `creator`, `create_time`, `password`, `remark`, `icon`) VALUES ('24', 'iris', '23', '1', '1', NOW(), 'F4AC0B61C74036ECF7528203F0EC7554', NULL, NULL);

-- 表單角色用戶(測試)
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('1', '1', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('12', '15', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('2', '14', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('3', '7', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('24', '2', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('5', '5', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('6', '3', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('7', '11', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('8', '10', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('9', '12', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('10', '9', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('3', '4', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('11', '16', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('5', '4', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('13', '13', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('14', '6', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('15', '8', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('15', '4', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('5', '17', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('7', '18', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('2', '19', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('13', '19', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('2', '20', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('2', '21', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('16', '22', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('17', '11', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('17', '18', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('19', '4', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('20', '4', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('21', '4', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('22', '14', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('22', '19', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('22', '20', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('22', '21', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('23', '14', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('23', '19', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('23', '20', '1');
INSERT INTO `form_user_role` (`user_id`, `role_id`, `status`) VALUES ('23', '21', '1');

