-- 公司(company)
DELETE FROM company;
ALTER TABLE company AUTO_INCREMENT = 1;

INSERT INTO `eform`.`company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('1', '伊凡達科技股份有限公司', '54140696', '臺北市松山區光復南路35號4樓之1', '02-77181681', '劉孝椽', '2013-12-19', NULL, NULL, '1', NOW(), '1');
INSERT INTO `eform`.`company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('2', '風靡資訊股份有限公司', '43846035', '臺北市松山區光復南路35號4樓之1', '02-77181681', '蕭伊婷', '2016-05-25', NULL, NULL, '1', NOW(), '1');
INSERT INTO `eform`.`company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('3', '樂服股份有限公司', '43836698', '臺北市松山區光復南路35號4樓之1', '02-77181681', '劉孝椽', '2016-05-20', NULL, NULL, '1', NOW(), '1');
INSERT INTO `eform`.`company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('4', '伊凡達科技(香港)遊戲股份有限公司', '1892756', 'N/A', 'N/A', 'N/A', '2013-04-16', '1', '2016-10-14 14:53:47', '1', NOW(), '1');
INSERT INTO `eform`.`company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('5', '牛番茄資訊(香港)股份有限公司', '2221280', 'N/A', 'N/A', 'N/A', '2015-04-09', NULL, NULL, '1', NOW(), '1');
INSERT INTO `eform`.`company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('6', '牛番茄資訊股份有限公司', '54368159', 'N/A', 'N/A', 'N/A', '2013-10-17', NULL, NULL, '1', NOW(), '1');
INSERT INTO `eform`.`company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('7', '香港商伊凡達股份有限公司台灣分公司', '43479410', 'N/A', 'N/A', 'N/A', '2016-04-06', NULL, NULL, '1', NOW(), '1');
INSERT INTO `eform`.`company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('8', '伊凡達科技(香港)股份有限公司', 'N/A', 'N/A', 'N/A', 'N/A', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `eform`.`company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('9', '鼎客科技股份有限公司', 'N/A', 'N/A', 'N/A', 'N/A', NULL, NULL, NULL, '1', NOW(), '1');
INSERT INTO `eform`.`company` (`id`, `name`, `tax_number`, `address`, `phone`, `owner`, `establish_date`, `modifier`, `update_time`, `creator`, `create_time`, `status`) VALUES ('10', '傾國科技股份有限公司', 'N/A', 'N/A', 'N/A', 'N/A', NULL, NULL, NULL, '1', NOW(), '1');

-- 部門(department)

-- 職稱(job_title)

-- 角色功能(role_function)
-- 總經理
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '6', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '7', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '8', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '9', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '10', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '11', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '12', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '17', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '21', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '22', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '23', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '24', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('2', '25', '1');

-- 副總經理
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '6', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '7', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '8', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '9', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '10', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '11', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '12', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '17', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '21', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '22', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '23', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '24', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('3', '25', '1');

-- 董事長
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('4', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('4', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('4', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('4', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('4', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('4', '17', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('4', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('4', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('4', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('4', '22', '1');

-- 行政主管
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '6', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '11', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '12', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '17', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('5', '22', '1');

-- 營運主管
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '14', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '17', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '21', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('6', '22', '1');

-- 技術主管
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '14', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '17', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '21', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('7', '22', '1');

-- 客服主管
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('8', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('8', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('8', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('8', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('8', '17', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('8', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('8', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('8', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('8', '21', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('8', '22', '1');

-- 商務主管
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('9', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('9', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('9', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('9', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('9', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('9', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('9', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('9', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('9', '22', '1');

-- 行銷主管
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('10', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('10', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('10', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('10', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('10', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('10', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('10', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('10', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('10', '22', '1');

-- 資訊主管
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('11', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('11', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('11', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('11', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('11', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('11', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('11', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('11', '21', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('11', '22', '1');

-- 出納
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('12', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('12', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('12', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('12', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('12', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('12', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('12', '19', '1');

--  總務
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('13', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('13', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('13', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('13', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('13', '18', '1');

--  會計
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('14', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('14', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('14', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('14', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('14', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('14', '17', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('14', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('14', '19', '1');

--  人資
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('15', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('15', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('15', '6', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('15', '7', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('15', '8', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('15', '9', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('15', '10', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('15', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('15', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('15', '18', '1');

--  產品
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('16', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('16', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('16', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('16', '14', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('16', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('16', '17', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('16', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('16', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('16', '21', '1');

--  技術
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('17', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('17', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('17', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('17', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('17', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('17', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('17', '21', '1');

--  客服
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('18', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('18', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('18', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('18', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('18', '17', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('18', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('18', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('18', '21', '1');

--  測試
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('19', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('19', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('19', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('19', '14', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('19', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('19', '16', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('19', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('19', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('19', '23', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('19', '24', '1');

--  法務
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('20', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('20', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('20', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('20', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('20', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('20', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('20', '22', '1');

--  一般
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('21', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('21', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('21', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('21', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('21', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('21', '19', '1');

-- 韓國辦公室
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('22', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('22', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('22', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('22', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('22', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('22', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('22', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('22', '22', '1');

-- 牛番茄
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('23', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('23', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('23', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('23', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('23', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('23', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('23', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('23', '22', '1');

-- 鼎客
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('24', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('24', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('24', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('24', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('24', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('24', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('24', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('24', '22', '1');

-- 風靡
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('25', '1', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('25', '4', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('25', '13', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('25', '15', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('25', '18', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('25', '19', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('25', '20', '1');
INSERT INTO `eform`.`t_erp_role_function` (`role_id`, `function_id`, `status`) VALUES ('25', '22', '1');

-- 用戶角色(uesr_role)

-- 員工(employee)

-- 用戶(user)

-- 用戶多員工
-- delete from t_erp_user_employee;
-- ALTER TABLE t_erp_user_employee AUTO_INCREMENT = 1;

