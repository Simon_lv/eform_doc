-- �Ǹ�
CREATE TABLE `sequence_data` (
  `sequence_name` varchar(100) NOT NULL,
  `sequence_date` varchar(8) NOT NULL,
  `sequence_increment` int(11) unsigned NOT NULL DEFAULT '1',
  `sequence_min_value` int(11) unsigned NOT NULL DEFAULT '1',
  `sequence_max_value` int(11) unsigned NOT NULL DEFAULT '99',
  `sequence_cur_value` int(11) unsigned DEFAULT '1',
  `sequence_cycle` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sequence_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- table create skip --

-- float to decimal
ALTER TABLE `receipt`
MODIFY COLUMN `amount` decimal(16,2) NOT NULL AFTER `description`;

ALTER TABLE `payment_form`
MODIFY COLUMN `total_amount` decimal(16,0) NOT NULL AFTER `contact_tel`;

ALTER TABLE `business_card_form`
MODIFY COLUMN `total_amount` decimal(16,0) NOT NULL AFTER `contact_tel`;

ALTER TABLE `receipt`
MODIFY COLUMN `rate` decimal(7,4) NOT NULL AFTER `currency`;
